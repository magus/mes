;;; init.el --- The top of my configuration -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(setq mes-location (if (member (system-name) '("imladris"))
                       'work 'home))

(let ((file-name-handler-alist nil))
  (dolist (mod '(mes-basics
                 mes-pkgmgmt
                 mes-usability
                 mes-mes
                 mes-completion
                 mes-term
                 mes-misc
                 mes-org
                 mes-latex
                 mes-email
                 mes-projects
                 mes-dev-basics
                 ;; mes-dev-cc
                 mes-dev-elisp
                 ;; mes-dev-hs-orig
                 mes-dev-hs-new
                 mes-dev-nix
                 mes-dev-py
                 mes-dev-misc
                 mes-dev-misc-home
                 mes-dev-misc-work
                 mes-ai))
    (require mod)))

;;; init.el ends here
