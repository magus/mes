;;; mes-dev-misc.el --- Setup for development in various languages -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package json-ts-mode
  :hook
  (json-ts-mode . electric-pair-local-mode)
  (json-ts-mode . combobulate-mode)
  :mode
  ("\\.json\\'" . json-ts-mode)
  :init
  (add-to-list 'treesit-language-source-alist '(json "https://github.com/tree-sitter/tree-sitter-json"))
  ;; (treesit-install-language-grammar 'json)
  (add-to-list 'major-mode-remap-alist '(json-mode . json-ts-mode))
  (defalias 'json-mode 'json-ts-mode) ;; without it I'll have to write json-ts in orgmode
  :general
  (mes/despot-def json-ts-mode-map
    "=" `("format" . ,(make-sparse-keymap))
    "=b" '("buffer" . json-pretty-print-buffer)
    "=B" '("buffer ord" . json-pretty-print-buffer-ordered)
    "=r" '("region" . json-pretty-print)
    "=R" '("region ord" . json-pretty-print-ordered)))

(use-package yaml-mode
  :after origami
  :hook
  (yaml-mode . electric-pair-local-mode)
  (yaml-mode . origami-mode)
  :general
  (mes/despot-def yaml-mode-map
    "i" '("indent" . indent-tools-hydra/body)))

(use-package dockerfile-mode)

(use-package sql)

(use-package sql-indent
  :hook (sql-mode . sqlind-minor-mode))

(provide 'mes-dev-misc)
;;; mes-dev-misc.el ends here
