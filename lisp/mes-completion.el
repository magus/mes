;;; mes-completion.el --- Setup of completion and related packages-*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package vertico
  :straight (:files (:defaults "extensions/*")
             :includes (vertico-buffer
                        vertico-directory
                        vertico-quick))
  :init
  (setq vertico-cycle t
        vertico-count 20)
  (vertico-mode)
  :general
  (general-def vertico-map
    "C-j" 'vertico-next
    "C-k" 'vertico-previous))

(use-package vertico-buffer
  :after vertico
  :config
  (customize-set-variable 'vertico-buffer-display-action
                          '(display-buffer-below-selected (window-height . 13)))
  (vertico-buffer-mode))

(use-package vertico-directory
  :after vertico
  :general
  (general-def vertico-map
    "C-h" 'vertico-directory-up
    "RET" 'vertico-directory-enter
    "DEL" 'vertico-directory-delete-char
    "M-DEL" 'vertico-directory-delete-word))

(use-package vertico-quick
  :after vertico
  :general
  (general-def vertico-map
    "M-j" 'vertico-quick-insert))

(use-package orderless
  :custom
  (orderless-matching-styles '(orderless-literal orderless-regexp orderless-flex))
  (completion-styles '(orderless partial-completion basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion)))))

(use-package marginalia
  :init
  (marginalia-mode)
  :general
  (general-def minibuffer-local-map
    "M-A" 'marginalia-cycle))

(use-package consult
  :init
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  (advice-add #'register-preview :override #'consult-register-window)
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :custom
  (completion-in-region-function #'consult-completion-in-region)
  :general
  (mes/tyrant-def
    "/" '("search" . consult-ripgrep)
    "bb" '("switch" . consult-buffer)
    "fF" '("fd" . consult-fd)
    "ji" '("idx" . consult-imenu)
    "sl" '("line" . consult-line)))

(use-package consult-yasnippet
  :general
  (mes/tyrant-def
    "xs" '("snippet" . consult-yasnippet)
    "Eyv" '("visit" . consult-yasnippet-visit-snippet-file)))

(use-package embark
  :init
  (setq embark-indicators
        '(embark-which-key-indicator
          embark-highlight-indicator
          embark-isearch-highlight-indicator))
  :config
  (defun embark-which-key-indicator ()
    "An embark indicator that displays keymaps using which-key.
The which-key help message will show the type and value of the
current target followed by an ellipsis if there are further
targets."
    (lambda (&optional keymap targets prefix)
      (if (null keymap)
          (which-key--hide-popup-ignore-command)
        (which-key--show-keymap
         (if (eq (plist-get (car targets) :type) 'embark-become)
             "Become"
           (format "Act on %s '%s'%s"
                   (plist-get (car targets) :type)
                   (embark--truncate-target (plist-get (car targets) :target))
                   (if (cdr targets) "…" "")))
         (if prefix
             (pcase (lookup-key keymap prefix 'accept-default)
               ((and (pred keymapp) km) km)
               (_ (key-binding prefix 'accept-default)))
           keymap)
         nil nil t (lambda (binding)
                     (not (string-suffix-p "-argument" (cdr binding))))))))
  (defun embark-hide-which-key-indicator (fn &rest args)
    "Hide the which-key indicator immediately when using the completing-read prompter."
    (which-key--hide-popup-ignore-command)
    (let ((embark-indicators
           (remq #'embark-which-key-indicator embark-indicators)))
      (apply fn args)))
  (advice-add #'embark-completing-read-prompter
              :around #'embark-hide-which-key-indicator)
  :general
  (general-unbind 'normal "C-.")
  (general-def
    :keymaps 'global-map
    "C-." 'embark-act
    "C-;" 'embark-dwim)
  (mes/tyrant-def
    "hB" 'embark-bindings))

(use-package embark-consult
  :after (consult embark)
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package corfu
  :straight (:files (:defaults "extensions/*.el")
             :includes (corfu-popupinfo
                        corfu-quick))
  :after orderless
  :init
  (global-corfu-mode)
  :custom
  (corfu-auto t)
  (corfu-auto-delay 0.5)
  (corfu-auto-prefix 3)
  (corfu-cycle t)
  (corfu-on-exact-match nil)
  (corfu-quit-no-match t)
  :general
  (corfu-map
   "C-SPC" 'corfu-insert-separator))

(use-package corfu-popupinfo
  :init
  (corfu-popupinfo-mode)
  :custom
  (corfu-popupinfo-delay 0.5))

(use-package corfu-quick
  :after corfu
  :general
  (corfu-map
   "M-TAB" 'corfu-quick-complete))

(use-package nerd-icons-corfu
  :ensure t
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package nerd-icons-completion
  :defer nil
  :after marginalia
  :hook (marginalia-mode . nerd-icons-completion-marginalia-setup)
  :config
  (nerd-icons-completion-mode))

(use-package cape
  :general
  (general-define-key
   :kemaps '(insert normal)
   :prefix "M-c"
   "d" 'cape-dabbrev
   "f" 'cape-file
   "i" 'cape-ispell))

(use-package bufferlo
  :after consult
  :config
  (defvar bufferlo-consult--source-buffer
    `(:name "Other Buffers"
      :narrow   ?b
      :category buffer
      :face     consult-buffer
      :history  buffer-name-history
      :state    ,#'consult--buffer-state
      :items ,(lambda () (consult--buffer-query
                          :predicate #'bufferlo-non-local-buffer-p
                          :sort 'visibility
                          :as #'buffer-name)))
    "Non-local buffer candidate source for `consult-buffer'.")
  (defvar bufferlo-consult--source-local-buffer
    `(:name "Local Buffers"
      :narrow   ?l
      :category buffer
      :face     consult-buffer
      :history  buffer-name-history
      :state    ,#'consult--buffer-state
      :default  t
      :items ,(lambda () (consult--buffer-query
                          :predicate #'bufferlo-local-buffer-p
                          :sort 'visibility
                          :as #'buffer-name)))
    "Local buffer candidate source for `consult-buffer'.")
  (setq consult-buffer-sources '(consult--source-hidden-buffer
                                 bufferlo-consult--source-local-buffer
                                 bufferlo-consult--source-buffer
                                 consult--source-modified-buffer
                                 consult--source-recent-file
                                 consult--source-file-register
                                 consult--source-bookmark
                                 consult--source-project-buffer-hidden
                                 consult--source-project-recent-file-hidden))
  (bufferlo-mode  1))

(provide 'mes-completion)
;;; mes-completion.el ends here
