;;; mes-mes.el --- Definitions of some useful functions -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package emacs
  :config
  (defun mes/open-init-el ()
    "Open the user's init.el file."
    (interactive)
    (find-file (expand-file-name "init.el" user-emacs-directory)))

  (defun mes/alternate-window ()
    "Switch between current and last window."
    (interactive)
    (if-let* ((prev-wnd (get-mru-window nil t t)))
        (select-window prev-wnd)
      (user-error "No last window to switch to")))

  (defun mes/switch-to-buffer (buffer-or-name)
    "Switches to a buffer only if it exists"
    (interactive)
    (when-let* ((db (get-buffer buffer-or-name)))
      (switch-to-buffer db)))

  :general
  (mes/tyrant-def
    "Ei" '("open cfg" . mes/open-init-el)
    "w TAB" '("latest" . mes/alternate-window)))

(use-package emacs
  :config
  (defun mes/bg-alpha-get ()
    (or (alist-get 'alpha-background
                   (frame-parameters (selected-frame)))
        100))

  (defun mes/bg-alpha-set (alpha)
    (let ((a (cond ((> alpha 100) 100)
                   ((< alpha 0) 0)
                   (t alpha))))
      (set-frame-parameter nil 'alpha-background a)))

  (defun mes/bg-alpha-inc ()
    (interactive)
    (let ((new-alpha (+ (mes/bg-alpha-get)
                        10)))
      (mes/bg-alpha-set new-alpha)))

  (defun mes/bg-alpha-dec ()
    (interactive)
    (let ((new-alpha (- (mes/bg-alpha-get)
                        10)))
      (mes/bg-alpha-set new-alpha)))

  (defun mes/bg-alpha-set-default ()
    (interactive)
    (mes/bg-alpha-set 100))

  :pretty-hydra
  (hydra-bg-opaqueness (:title "Background transparency" :quit-key "q")
                       ("Transparency" (("+" mes/bg-alpha-dec "inc")
                                        ("-" mes/bg-alpha-inc "dec"))
                        "Ops" (("0" mes/bg-alpha-set-default "reset" :exit t))))
  :general
  (mes/tyrant-def
    "Et" '("transparency" . hydra-bg-opaqueness/body)))

(defun mes/auth-get-pwd (host)
  "Get the password for a host (authinfo.gpg)"
  (-> (auth-source-search :host host)
      car
      (plist-get :secret)
      funcall))

(defun mes/auth-get-key (host key)
  "Get a key's value for a host (authinfo.gpg)

Not usable for getting the password (:secret), use 'mes/auth-get-pwd'
for that."
  (-> (auth-source-search :host host)
      car
      (plist-get key)))

(provide 'mes-mes)
;;; mes-mes.el ends here
