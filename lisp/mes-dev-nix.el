;;; mes-dev-nix.el --- Setup for nix development -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package nix-mode
  :general
  (mes/despot-def nix-mode-map
    "=" `("format" . ,(make-sparse-keymap))
    "=b" '("buffer" . nix-format-buffer)))

(provide 'mes-dev-nix)
;;; mes-dev-nix.el ends here
