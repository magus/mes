;;; mes-usability.el --- Various packages for general usability -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package evil
  :init
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-want-C-u-scroll t)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal)
  :custom
  (evil-goto-definition-functions '(evil-goto-definition-xref
                                    evil-goto-definition-imenu
                                    evil-goto-definition-semantic
                                    evil-goto-definition-search)))

(use-package general
  :after evil
  :config
  (general-evil-setup)
  (general-auto-unbind-keys)

  (general-define-key
   :states '(normal insert motion visual emacs)
   :keymaps 'override
   :prefix-map 'tyrant-map
   :prefix "SPC"
   :non-normal-prefix "M-SPC")
  (general-create-definer mes/tyrant-def :keymaps 'tyrant-map)
  (mes/tyrant-def "" nil)

  (general-create-definer mes/despot-def
    :states '(normal insert motion visual emacs)
    :keymaps 'override
    :major-modes t
    :prefix ","
    :non-normal-prefix "M-,")
  (mes/despot-def "" nil)

  (general-def universal-argument-map
    "SPC u" 'universal-argument-more))

(use-package hydra)

(use-package pretty-hydra
  :after (hydra))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package anzu
  :init
  (global-anzu-mode +1))

(use-package evil-anzu
  :after evil)

(use-package evil-commentary
  :after evil
  :config
  (evil-commentary-mode))

(use-package evil-snipe
  :after evil
  :hook (magit-mode . turn-off-evil-snipe-override-mode)
  :config
  (evil-snipe-mode 1)
  (evil-snipe-override-mode 1))

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package evil-matchit
  :config (global-evil-matchit-mode 1))

(use-package evil-unimpaired
  :after evil
  :straight (:host github
             :repo "zmaas/evil-unimpaired")
  :config
  (evil-unimpaired-mode))

(use-package evil-mc
  :after (evil general which-key)
  :config
  (global-evil-mc-mode 1)

  (general-def evil-mc-key-map
    :states '(normal visual)
    "g.A" '("make end sel" . evil-mc-make-cursor-in-visual-selection-end)
    "g.I" '("make beg sel" . evil-mc-make-cursor-in-visual-selection-beg)
    "g.a" '("make all" . evil-mc-make-all-cursors)
    "g.q" '("undo all" . evil-mc-undo-all-cursors)
    "g.u" '("undo last" . evil-mc-undo-last-added-cursor)
    "g. RET" '("make here" . evil-mc-make-cursor-here)
    "g.p" '("pause" . evil-mc-pause-cursors)
    "g.r" '("resume" . evil-mc-resume-cursors)

    "g.m" `("make & go" . ,(make-sparse-keymap))
    "g.m$" '("to last cur" . evil-mc-make-and-goto-last-cursor)
    "g.m0" '("to first cur" . evil-mc-make-and-goto-first-cursor)
    "g.mC" '("to prev cur" . evil-mc-make-and-goto-prev-cursor)
    "g.mc" '("to next cur" . evil-mc-make-and-goto-next-cursor)
    "g.mh" '("to prev match" . evil-mc-make-and-goto-prev-match)
    "g.mj" '("to next line" . evil-mc-make-cursor-move-next-line)
    "g.mk" '("to prev line" . evil-mc-make-cursor-move-prev-line)
    "g.ml" '("to next match" . evil-mc-make-and-goto-next-match)

    "g.s" `("skip & go" . ,(make-sparse-keymap))
    "g.sC" '("to prev cur" . evil-mc-skip-and-goto-prev-cursor)
    "g.sc" '("to next cur" . evil-mc-skip-and-goto-next-cursor)
    "g.sh" '("to prev match" . evil-mc-skip-and-goto-prev-match)
    "g.sl" '("to next match" . evil-mc-skip-and-goto-next-match))

  (general-unbind '(normal visual) evil-mc-key-map
    "g.$" "g.0"
    "g. C-n" "g. C-S-n"
    "g. C-u" "g. C-S-u"
    "g. C-p" "g. C-r"
    "g. M-N" "g. M-n"
    "g.N" "g.O" "g.n" "g.o")

  (which-key-add-key-based-replacements "g." "mc"))

(use-package evil-lisp-state
  :after evil)

(use-package catppuccin-theme
  :init (load-theme 'catppuccin :no-confirm))

(use-package nerd-icons)

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

(use-package unicode-fonts
  :config
  (unicode-fonts-setup))

(use-package dashboard
  :init
  (setq
   dashboard-banner-logo-title "Magnus Emacs Setup"
   dashboard-startup-banner 'logo
   dashboard-center-content t
   dashboard-items '((recents . 15) (projects . 10))
   dashboard-set-heading-icons t
   dashboard-set-file-icons t
   dashboard-set-init-info t
   dashboard-projects-backend 'projectile)
  :config
  (dashboard-setup-startup-hook))

(use-package hl-todo
  :config
  (global-hl-todo-mode 1))

(use-package hl-line
  :config
  (global-hl-line-mode 1))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package which-key
  :after evil
  :config
  (which-key-mode)
  :custom
  (which-key-idle-delay 0.7)
  ;; (which-key-show-operator-state-maps t) ;; experimental and breaks things
  )

(use-package envrc
  :init (envrc-global-mode))

(use-package hydra-posframe
  :straight (:host github
             :repo "Ladicle/hydra-posframe")
  :hook (after-init . hydra-posframe-mode))

(use-package emacs
  :after (general pretty-hydra)
  :hook (prog-mode . (lambda () (setq-local imenu-auto-rescan t)))
  :custom
  (x-stretch-cursor t)
  :pretty-hydra
  (hydra-text-zoom (:title "Text zoom" :quit-key "q")
                   ("Scale" (("+" text-scale-increase "in")
                             ("-" text-scale-decrease "out"))
                    "Ops" (("0" (lambda() (interactive) (text-scale-set 0)) "reset" :exit t))))
  (hydra-win-ctrl (:title "Win ctrl" :quit-key "q")
                  ("Go" (("h" evil-window-left "←")
                         ("j" evil-window-down "↓")
                         ("k" evil-window-up "↑")
                         ("l" evil-window-right "→"))
                   "Size" (("S-<left>" shrink-window-horizontally "shrink ↔")
                           ("S-<right>" enlarge-window-horizontally "enlarge ↔")
                           ("S-<down>" shrink-window "shrink ↕")
                           ("S-<up>" enlarge-window "enlarge ↕"))
                   "Move" (("H" evil-window-move-far-left "⇤")
                           ("J" evil-window-move-very-bott "⤓")
                           ("K" evil-window-move-very-top "⤒")
                           ("L" evil-window-move-far-right "⇥"))
                   "Split" (("s" evil-window-split)
                            ("v" evil-window-vsplit)
                            ("=" balance-windows))
                   "Close" (("c" evil-window-delete)
                            ("o" delete-other-windows))))
  :config ;; basic keybindings
  (mes/tyrant-def
    "SPC" '("M-x" . execute-extended-command)
    "TAB" '("latest buffer" . mode-line-other-buffer)
    ;; "!" '("sh cmd" . shell-command)
    "!" '("sh cmd" . async-shell-command)
    "u" '("C-u" . universal-argument)

    "b" `("bufs" . ,(make-sparse-keymap))
    "bc" '("close" . (lambda () (interactive) (kill-buffer (current-buffer))))
    "bC" '("close w/ window" . kill-buffer-and-window)
    "be" '("erase" . erase-buffer)
    "bs" '("scratch" . scratch-buffer)
    "bd" '("dashboard" . dashboard-open)
    "bR" '("recover" . recover-this-file)

    "E" `("emacs" . ,(make-sparse-keymap))
    "ER" `("recursive" . ,(make-sparse-keymap))
    "ERa" '("abort" . abort-recursive-edit)
    "ERe" '("exit" . exit-recursive-edit)
    "Ed" `("direnv" . ,(make-sparse-keymap))
    "Eda" '("allow" . envrc-allow)
    "Edd" '("deny" . envrc-deny)
    "Edu" '("update" . envrc-reload)
    "Ep" `("pkgs" . ,(make-sparse-keymap))
    "EpU" '("update recipes" . straight-pull-recipe-repositories)
    "Epp" '("prune" . (lambda () (interactive) (straight-prune-build) (straight-remove-unused-repos)))
    "Epu" '("update" . straight-pull-all)
    "Epv" '("visit" . straight-visit-package)
    "Eq" '("quit" . save-buffers-kill-emacs)
    "Er" '("restart" . restart-emacs)
    "Ey" `("snippets" . ,(make-sparse-keymap))
    "Eyn" '("new" . yas-new-snippet)

    "f" `("files" . ,(make-sparse-keymap))
    "ff" '("find" . find-file)
    "fi" '("insert" . insert-file)
    "fs" '("save" . save-buffer)
    "fS" '("save all" . evil-write-all)
    "fy" '("copy name" . (lambda () (interactive) (kill-new (buffer-file-name))))

    "h" `("help" . ,(make-sparse-keymap))
    "hb" '("binding" . describe-bindings)
    "hf" '("func" . describe-function)
    "hF" '("face" . describe-face)
    "hi" '("info man" . info-display-manual)
    "hk" '("key" . describe-key)
    "hm" '("mode" . describe-mode)
    "hv" '("var" . describe-variable)
    "hw" '("woman" . consult-man)
    "ht" '("top bindings" . which-key-show-top-level)

    "j" `("jump" . ,(make-sparse-keymap))
    "jc" '("char timer" . evil-avy-goto-char-timer)
    "jl" '("line" . evil-avy-goto-line)

    "n" `("narrow" . ,(make-sparse-keymap))
    "nr" '("region" . narrow-to-region)
    "nr" '("region" . narrow-to-region)
    "np" '("page" . narrow-to-page)
    "nf" '("function" . narrow-to-defun)
    "nw" '("function" . widen)

    "p" `("project" . ,(make-sparse-keymap))
    "p/" '("search proj" . consult-ripgrep)

    "w" `("win" . ,(make-sparse-keymap))
    "w TAB" '("prev" . evil-window-prev)
    "w=" '("balance" . balance-windows)
    "wC" '("win ctrl" . hydra-win-ctrl/body)
    "wh" '("←" . evil-window-left)
    "wj" '("↓" . evil-window-down)
    "wk" '("↑" . evil-window-up)
    "wl" '("→" . evil-window-right)
    "ws" '("split below" . evil-window-split)
    "wv" '("split right" . evil-window-vsplit)
    "wS" '("split below & focus" . (lambda () (interactive) (evil-window-split) (evil-window-down 1)))
    "wV" '("split rigth & focus" . (lambda () (interactive) (evil-window-vsplit) (evil-window-right 1)))

    "s" `("search" . ,(make-sparse-keymap))
    "so" '("occur" . occur)

    "t" `("tabs" . ,(make-sparse-keymap))
    "t TAB" '("latest" . tab-recent)
    "tc" '("close" . tab-close)
    "th" '("previous" . tab-previous)
    "tl" '("next" . tab-next)
    "tn" '("new" . (lambda () (interactive) (tab-new) (dashboard-open)))
    "tr" '("rename" . tab-rename)

    "x" `("text" . ,(make-sparse-keymap))
    "xi" '("ins char" . insert-char)
    "xI" '("ins nerd" . nerd-icons-insert)
    "xz" '("txt zoom" . hydra-text-zoom/body)

    "a" `("apps" . ,(make-sparse-keymap))
    "e" `("errors" . ,(make-sparse-keymap))
    "g" `("git" . ,(make-sparse-keymap))
    "k" `("lisp" . ,(make-sparse-keymap)))

  (general-define-key
   :states 'motion
   "#" 'evil-ex-search-word-forward
   "*" 'evil-ex-search-word-backward
   "/" 'evil-ex-search-forward
   "?" 'evil-ex-search-backward
   "N" 'evil-ex-search-previous
   "n" 'evil-ex-search-next
   "g#" 'evil-ex-search-unbounded-word-forward
   "g*" 'evil-ex-search-unbounded-word-backward))

(use-package helpful
  :general
  (general-define-key
   :keymaps 'global-map
   [remap describe-command] 'helpful-command
   [remap describe-function] 'helpful-callable
   [remap describe-key] 'helpful-key
   [remap describe-variable] 'helpful-variable
   [remap describe-symbol] 'helpful-symbol))

(use-package yasnippet
  :init (yas-global-mode 1)
  :hook
  ((text-mode prog-mode) . yas-minor-mode-on)
  :config
  (add-to-list 'warning-suppress-types '(yasnippet backquote-change))
  :general
  (mes/despot-def snippet-mode-map
    "c" '("load & close" . yas-load-snippet-buffer-and-close)
    "l" '("load" . yas-load-snippet-buffer)
    "t" '("try" . yas-tryout-snippet)))

(use-package yasnippet-snippets)

(use-package wgrep
  :general
  (mes/despot-def grep-mode-map
    "i" '("wgrep-mode" . wgrep-change-to-wgrep-mode))
  (mes/despot-def wgrep-mode-map
    "c" '("finish" . wgrep-finish-edit)
    "d" '("delete" . wgrep-mark-deletion)
    "k" '("abort" . wgrep-abort-changes)
    "q" '("quit" . wgrep-exit)
    "u" `("undo" . ,(make-sparse-keymap))
    "ua" '("all" . wgrep-remove-all-change)
    "ur" '("region" . wgrep-remove-change)))

(use-package dirvish ;; and dired
  :init
  (dirvish-override-dired-mode)
  :custom
  (dired-recursive-deletes 'always)
  (dired-recursive-copies 'always)
  (delete-by-moving-to-trash t)
  (dired-dwim-target t)
  (dirvish-attributes '(vc-state subtree-state nerd-icons collapse git-msg file-time file-size))
  :general
  (general-def dired-mode-map
    "<tab>" 'dirvish-subtree-toggle)
  (mes/despot-def dired-mode-map
    "i" '("insert subdir" . dired-maybe-insert-subdir)
    "k" '("kill subdir" . dired-kill-subdir)
    "r" '("visit rubbish" . trashed)))

(use-package trashed
  :commands (trashed)
  :custom
  (trashed-action-confirmer 'y-or-n-p)
  (trashed-use-header-line t))

(use-package mep-manacle
  :straight (:type git
             :repo "git@gitlab.com:magus/mep-manacle.git"
             :branch "main")
  :custom
  (display-buffer-alist `((,(mep-manacle-make-match-mode-function '(help-mode helpful-mode Info-mode))
                           (display-buffer-reuse-mode-window display-buffer-in-side-window)
                           (mode help-mode helpful-mode)
                           (window-width . 0.33)
                           (side . right))
                          (,(mep-manacle-make-match-mode-function '(grep-mode occur-mode))
                           (display-buffer-reuse-mode-window display-buffer-in-side-window)
                           (mode grep-mode occur-mode)
                           (window-height . 0.25)
                           (side . bottom))
                          (,(mep-manacle-make-match-mode-function '(compilation-mode))
                           (display-buffer-reuse-mode-window display-buffer-in-side-window)
                           (mode compilation-mode)
                           (window-height . 0.25)
                           (side . bottom)
                           (post-command-select-window . t))
                          (,(mep-manacle-make-match-mode-function '(copilot-chat-mode))
                           (display-buffer-reuse-mode-window display-buffer-in-direction)
                           (mode help-mode copilot-chat-mode)
                           (window-width . 0.5)
                           (direction . right))
                          (,(mep-manacle-make-match-mode-function
                             '(flycheck-error-list-mode flycheck-projectile-error-list-mode shell-mode special-mode))
                           (display-buffer-reuse-mode-window display-buffer-in-side-window)
                           ;; skip mode to re-use based on buffer's mode
                           (window-height . 0.25)
                           (side . bottom))
                          (,(rx (seq "*" "Warnings" "*"))
                           ;; (display-buffer-in-direction)
                           (window-height . 0.20)
                           (direction . down)))))

(use-package ace-window
  :custom
  (aw-dispatch-always 't)
  (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  :general
  (mes/tyrant-def
    "wc" '("close" . ace-delete-window)
    "wo" '("close other" . ace-delete-other-windows)
    "ww" '("ace" . ace-window)))

(use-package tab-bar
  :config (tab-bar-history-mode 1)
  :init (setq tab-bar-history-limit 100)
  :general
  (mes/tyrant-def
    "wu" '("undo" . tab-bar-history-back)
    "wr" '("redo" . tab-bar-history-forward)))

;; (use-package ispell)

(use-package flyspell
  :hook (text-mode . flyspell-mode)
  :custom (flyspell-issue-message-flag nil))

(use-package symbol-overlay
  :general
  (mes/tyrant-def
    "S" `("syms" . ,(make-sparse-keymap))
    "Sc" '("clear" . symbol-overlay-remove-all)
    "Sh" '("back" . symbol-overlay-switch-backward)
    "Sj" '("next" . symbol-overlay-jump-next)
    "Sk" '("prev" . symbol-overlay-jump-prev)
    "Sl" '("fwd" . symbol-overlay-switch-forward)
    "Ss" '("hi" . symbol-overlay-put)
    "St" '("mode" . symbol-overlay-mode)
    "Sm" '("mv" . symbol-overlay-rename)
    "SM" '("mv?" . symbol-overlay-query-replace)))

(provide 'mes-usability)
;;; mes-usability.el ends here
