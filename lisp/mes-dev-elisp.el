;;; mes-dev-elisp.el --- Setup for emacs-lisp development -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package elisp-mode
  :straight nil
  :ensure nil
  :hook
  (emacs-lisp-mode . aggressive-indent-mode)
  :config
  (put 'elpaca 'lisp-indent-function 'defun)
  :general
  (mes/despot-def (emacs-lisp-mode-map lisp-interaction-mode-map)
    "b" '("eval buffer" . eval-buffer)
    "g" `("goto" . ,(make-sparse-keymap))
    "gb" '("pop" . pop-tag-mark)
    "gd" '("goto defs" . evil-goto-definition)
    "gr" '("find refs" . xref-find-references)
    "k" '("kill sexp" . kill-sexp)
    "r" '("eval region" . eval-region)))

(use-package lisp-keyword-indent
  :straight (:host github
             :repo "twlz0ne/lisp-keyword-indent.el")
  :config
  (setq lisp-indent-function 'lisp-indent-function)
  (lisp-keyword-indent-mode 1))

(use-package eros
  :general
  (mes/despot-def (emacs-lisp-mode-map lisp-interaction-mode-map)
    "e" '("eval sexp" . eros-eval-last-sexp)
    "f" '("eval defun" . eros-eval-defun)
    "i" '("inspect" . eros-inspect-last-result)))

(use-package paredit
  :hook
  (emacs-lisp-mode . enable-paredit-mode)
  (eval-expression-minibuffer-setup . enable-paredit-mode))

(provide 'mes-dev-elisp)
;;; mes-dev-elisp.el ends here
