;;; mes-projects.el --- Setup for a project workflow -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package projectile
  :custom
  (projectile-switch-project-action 'projectile-commander)
  :config
  (projectile-global-mode +1)
  (defun mes/kill-project () (interactive) (projectile-kill-buffers))
  :general
  (mes/tyrant-def
    "pA" '("add proj" . projectile-add-known-project)
    "pC" '("close bufs+tab" . (lambda () (interactive) (mes/kill-project) (tab-close)))
    "pE" '("edit dirlocals" . projectile-edit-dir-locals)
    "pa" '("toggle test" . projectile-toggle-between-implementation-and-test)
    "pc" '("close bufs" . mes/kill-project)
    "pd" '("root dired" . projectile-dired)
    "pr" `("run" . ,(make-sparse-keymap))
    "prc" '("compile" . projectile-compile-project)
    "prt" '("tests" . projectile-test-project)))

(use-package consult-projectile
  :init
  (setq consult-projectile-use-projectile-switch-project t)
  :general
  (mes/tyrant-def
    "pf" '("find file" . consult-projectile-find-file)
    "po" '("open" . consult-projectile)))

(use-package mep-projectile-extras
  :straight (:type git
             :repo "git@gitlab.com:magus/mep-projectile-extras.git"
             :branch "main")
  :after projectile
  :init
  (setq xref-history-storage #'mep-projectile-extras-xref-history)
  :config
  (advice-add 'mes/kill-project :before #'mep-projectile-extras-kill-project-store)
  :general
  (mes/tyrant-def
    "p!" '("sh cmd" . mep-projectile-async-shell-command)
    "pt" '("todo" . mep-projectile-open-todo)))

(provide 'mes-projects)
;;; mes-projects.el ends here
