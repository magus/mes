;;; mes-misc.el --- Setup of packages that don't fit anywhere else (yet) -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package markdown-mode
  :general
  (mes/despot-def markdown-mode-map
    ;; insert
    "i" `("insert" . ,(make-sparse-keymap))
    "i-" '("hr" . markdown-insert-hr)
    "if" '("footnote" . markdown-insert-footnote)
    "ii" '("image" . markdown-insert-image)
    "il" '("link" . markdown-insert-link)
    "l" `("list" . ,(make-sparse-keymap))
    "l#" '("re-num" . markdown-cleanup-list-numbers)
    "lH" '("unindent" . markdown-promote-list-item)
    "lJ" '("mv down" . markdown-move-list-item-down)
    "lK" '("mv up" . markdown-move-list-item-up)
    "lL" '("indent" . markdown-demote-list-item)
    "lj" '("end" . markdown-end-of-list)
    "lk" '("beginning" . markdown-beginning-of-list)
    ;; tables
    "t" `("table" . ,(make-sparse-keymap))
    "tn" '("new" . markdown-insert-table)
    "ta" '("align" . markdown-table-align)
    "tk" '("mv row up" . markdown-table-move-row-up)
    "tj" '("mv row down" . markdown-table-move-row-down)
    "tl" '("mv col right" . markdown-table-move-column-right)
    "th" '("mv col left" . markdown-table-move-column-left)
    "tr" '("ins row" . markdown-table-insert-row)
    "tR" '("del row" . markdown-table-delete-row)
    "tc" '("ins col" . markdown-table-insert-column)
    "tC" '("del col" . markdown-table-delete-column)
    "ts" '("sort" . markdown-table-sort-lines)
    "td" '("convert" . markdown-table-convert-region)
    "tt" '("transpose" . markdown-table-transpose)
    ;; text
    "x" `("text" . ,(make-sparse-keymap))
    "xb" '("bold" . markdown-insert-bold)
    "xc" '("code" . markdown-insert-code)
    "xi" '("italic" . markdown-insert-italic)
    "xs" '("strike-through" . markdown-insert-strike-through)
    "xB" '("checkbox" . markdown-insert-gfm-checkbox)
    "xC" '("code block" . markdown-insert-gfm-code-block)
    "xq" '("quote" . markdown-insert-blockquote)
    "xQ" '("quote region" . markdown-blockquote-region)
    "xp" '("pre" . markdown-insert-pre)
    "xP" '("pre region" . markdown-pre-region)))

(use-package graphviz-dot-mode
  :config
  (setq graphviz-dot-indent-width 4))

(use-package mermaid-mode)

(use-package systemd)

(use-package csv-mode
  :hook (csv-mode . csv-align-mode)
  :general
  ;; TODO: evil overrides TAB, how do I prevent that?
  (mes/despot-def csv-mode-map
    "TAB" '("next col" . csv-tab-command)
    "<backtab>" '("prev col" . csv-backtab-command)
    "a" '("align" . csv-align-fields)
    "k" '("kill" . csv-kill-fields)
    "n" '("sort num" . csv-sort-numeric-fields)
    "s" '("sort" . csv-sort-fields)
    "t" '("transpose" . csv-transpose)
    "u" '("unalign" . csv-unalign-fields)))

(use-package vimrc-mode)

(use-package kdl-mode
  :straight (:host github
             :repo "bobuk/kdl-mode"))

(provide 'mes-misc)
;;; mes-misc.el ends here
