;;; mes-ai.el --- Setup of AI/LLM stuff -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package gptel
  :custom
  (gptel-default-mode 'org-mode)
  (gptel-max-tokens 2000)
  (gptel-model 'gpt-4o)
  :general
  (mes/tyrant-def
    "ag" '("chatgpt" . gptel))
  (mes/despot-def gptel-mode-map
    "RET" '("chatgpt send" . gptel-send)
    "?" '("chatgpt menu" . gptel-menu)))

(use-package aidermacs
  :straight (:host github :repo "MatthewZMD/aidermacs" :files ("*.el"))
  :config
  (setq aidermacs-args '("--model" "gpt-4o"))
  (mes/tyrant-def
    "aa" '("aider" . aidermacs-transient-menu)))

(provide 'mes-ai)
;;; mes-ai.el ends here
