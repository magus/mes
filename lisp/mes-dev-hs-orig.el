;;; mes-dev-hs-orig.el --- Setup for haskell development (haskell-mode) -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; it seems haskell-mode sets this no matter what and fails when it's void
(setq flymake-allowed-file-name-masks nil)

(use-package haskell-mode
  :after reformatter
  :hook
  (haskell-mode . lsp-deferred)
  (haskell-mode . (lambda () (setq-local tab-width 4)))
  :config ;; - set formatting to use fourmolu
  (reformatter-define cabal-format
    :program "cabal-fmt"
    :args '("/dev/stdin"))
  :general
  (mes/despot-def haskell-mode-map
    "'" '("interactive" . haskell-interactive-switch)
    "=" `("format" . ,(make-sparse-keymap))
    "=b" '("buf" . lsp-format-buffer)
    "=r" '("region" . lsp-format-region)
    "a" `("code actions" . ,(make-sparse-keymap))
    "aa" '("code action" . lsp-execute-code-action)
    "al" '("lens" . lsp-avy-lens)
    "b" `("backend" . ,(make-sparse-keymap))
    ;; "bc" '("connections" . ??) - list lsp connections
    "br" '("restart" . lsp-workspace-restart)
    "bs" '("shutdown" . lsp-workspace-shutdown)
    ;; "c" `("cabal" . ,(make-sparse-keymap))
    "g" `("goto" . ,(make-sparse-keymap))
    "gb" '("pop" . pop-tag-mark)
    "gc" '("goto cabal" . haskell-cabal-visit-file)
    "gd" '("goto def" . evil-goto-definition)
    "gr" '("find refs" . xref-find-references)
    "gt" '("find type def" . lsp-find-type-definition)
    "gT" '("goto type def" . lsp-goto-type-definition)
    "gi" '("to imports" . (lambda () (interactive) (xref-push-marker-stack) (haskell-navigate-imports)))
    ;; "gi" '("find impl" . lsp-find-implementation)
    ;; "gI" '("goto impl" . lsp-goto-implementation)
    "h" `("help" . ,(make-sparse-keymap))
    "hd" '("describe" . lsp-describe-thing-at-point)
    "i" '("indent" . indent-tools-hydra/body)
    "r" `("refactor" . ,(make-sparse-keymap))
    ;; "re" '("extract" . extract?)
    ;; "ri" '("inline" . inline?)
    "rr" '("rename" . lsp-rename)
    "s" `("repl" . ,(make-sparse-keymap))
    "sb" '("send buf" . haskell-process-load-file)
    "sc" '("clear" . haskell-interactive-mode-clear)
    "st" '("change target" . haskell-session-change-target)
    "sS" '("bring interactive" . haskell-interactive-bring)
    ;; "x" `("text" . ,(make-sparse-keymap))
    ;; "d" `("debug" . ,(make-sparse-keymap))
    ;; "F" `("folder" . ,(make-sparse-keymap))
    ;; "G" `("peek" . ,(make-sparse-keymap))
    "T" `("toggle" . ,(make-sparse-keymap))
    "Tl" '("lsp" . lsp))
  (mes/despot-def haskell-interactive-mode-map
    "'" '("back" . haskell-interactive-switch-back))
  (mes/despot-def haskell-cabal-mode-map
    "=" `("format" . ,(make-sparse-keymap))
    "=b" '("buf" . cabal-format-buffer)
    "=r" '("region" . cabal-format-region)
    "f" '("open source file" . haskell-cabal-find-or-create-source-file)
    "g" `("goto" . ,(make-sparse-keymap))
    "g[" '("subsection beginning" . haskell-cabal-beginning-of-subsection)
    "g]" '("subsection end" . haskell-cabal-end-of-subsection)
    "gb" '("benchmark section" . haskell-cabal-goto-benchmark-section)
    "gc" '("common section" . haskell-cabal-goto-common-section)
    "ge" '("exe section" . haskell-cabal-goto-executable-section)
    "gJ" '("next section" . haskell-cabal-next-section)
    "gj" '("next section" . haskell-cabal-next-subsection)
    "gK" '("prev section" . haskell-cabal-previous-section)
    "gk" '("next section" . haskell-cabal-previous-subsection)
    "gl" '("lib section" . haskell-cabal-goto-library-section)
    "gt" '("text section" . haskell-cabal-goto-test-suite-section)
    "g{" '("section beginning" . haskell-cabal-beginning-of-section)
    "g}" '("section end" . haskell-cabal-end-of-section)
    "s" '("arrange subsection" . haskell-cabal-subsection-arrange-lines)))

(use-package lsp-haskell
  :hook
  (haskell-mode . lsp-deferred)
  :custom
  (lsp-haskell-server-path "haskell-language-server")
  (lsp-haskell-server-args '("--log-file" "/tmp/hls.log")))

(provide 'mes-dev-hs-orig)
;;; mes-dev-hs-orig.el ends here
