;;; mes-dev-hs-new.el --- Setup for haskell development (haskell-ng-mode) -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package haskell-ng-mode
  :straight (:type git
             :repo "git@gitlab.com:magus/haskell-ng-mode.git"
             :branch "main")
  :init
  (add-to-list 'treesit-language-source-alist '(haskell . ("https://github.com/tree-sitter/tree-sitter-haskell" "v0.23.1")))
  (add-to-list 'treesit-language-source-alist '(cabal "https://gitlab.com/magus/tree-sitter-cabal.git"))
  ;; (treesit-install-language-grammar 'haskell)
  ;; (treesit-install-language-grammar 'cabal)
  (add-to-list 'major-mode-remap-alist '(haskell-mode . haskell-ng-mode))
  (add-to-list 'major-mode-remap-alist '(cabal-mode . cabal-ng-mode))
  (defalias 'haskell-mode #'haskell-ng-mode)
  (defalias 'cabal-mode #'cabal-ng-mode)
  :hook
  (haskell-ng-mode . lsp-deferred)
  (haskell-ng-mode . (lambda () (setq-local tab-width 4)))
  :config
  (reformatter-define cabal-format
    :program "cabal-fmt"
    :args '("/dev/stdin"))
  (reformatter-define cabal-gild
    :program "cabal-gild")
  (defun cbl-fmt-buffer (&optional display-errors)
    (interactive "p")
    (cond ((executable-find "cabal-gild") (cabal-gild-buffer display-errors))
          ((executable-find "cabal-fmt") (cabal-format-buffer display-errors))
          (t (error "No formatting tool for Cabal found"))))
  (defun cbl-fmt-region (beg end &optional display-errors)
    (interactive "rp")
    (cond ((executable-find "cabal-gild") (cabal-gild-region beg end display-errors))
          ((executable-find "cabal-fmt") (cabal-format-region beg end display-errors))
          (t (error "No formatting tool for Cabal found"))))
  :custom
  (evil-shift-round nil)
  :general
  (mes/despot-def haskell-ng-mode-map
    "=" `("format" . ,(make-sparse-keymap))
    "'" '("repl" . haskell-ng-repl-run)
    "=b" '("buf" . lsp-format-buffer)
    "T" `("toggle" . ,(make-sparse-keymap))
    "Tl" '("lsp" . lsp)
    "a" `("code actions" . ,(make-sparse-keymap))
    "aa" '("code action" . lsp-execute-code-action)
    "al" '("lens" . lsp-avy-lens)
    "b" `("backend" . ,(make-sparse-keymap))
    "br" '("restart" . lsp-workspace-restart)
    "bs" '("shutdown" . lsp-workspace-shutdown)
    "g" `("goto" . ,(make-sparse-keymap))
    "gb" '("pop" . pop-tag-mark)
    "gd" '("goto def" . evil-goto-definition)
    "gr" '("find refs" . xref-find-references)
    "gt" '("find type def" . lsp-find-type-definition)
    "gT" '("goto type def" . lsp-goto-type-definition)
    "h" `("help" . ,(make-sparse-keymap))
    "hd" '("describe" . lsp-describe-thing-at-point)
    "i" '("indent" . indent-tools-hydra/body)
    "r" `("refactor" . ,(make-sparse-keymap))
    "rr" '("rename" . lsp-rename)
    "s" `("repl" . ,(make-sparse-keymap))
    "sb" '("send buf" . haskell-ng-repl-load-buffer)
    "sB" '("send buf" . haskell-ng-repl-load-buffer-and-go)
    "sr" '("send region" . haskell-ng-repl-send-region)
    "sR" '("send region" . haskell-ng-repl-send-region-and-go))
  (mes/despot-def cabal-ng-mode-map
    "=" `("format" . ,(make-sparse-keymap))
    "=b" '("buf" . cbl-fmt-buffer)
    "=r" '("region" . cbl-fmt-region)))

;; (use-package tree-sitter-indent
;;   :straight (:host github
;;              :repo "magthe/tree-sitter-indent"
;;              :branch "master"))

(use-package lsp-haskell
  :straight (:host github
             :repo "magthe/lsp-haskell"
             :branch "master")
  :custom
  (lsp-haskell-server-path "haskell-language-server")
  (lsp-haskell-server-args '("--log-file" "/tmp/hls.log"))
  (lsp-haskell-formatting-provider "fourmolu"))

(use-package ob-haskell-ng
  :straight (:host github :repo "tonyday567/ob-haskell-ng")
  :after haskell-ng-mode
  :config
  (add-to-list 'org-babel-load-languages '(haskell . t)))

(defun ts-inspect ()
  (interactive)
  (when-let* ((nap (treesit-node-at (point))))
    (message "%S - %S" nap (treesit-node-type nap))))

(defun ts-query-root (query)
  (interactive "sQuery: ")
  (let ((ss0 (treesit-query-capture (treesit-buffer-root-node) query)))
    (message "%S" ss0)))

(provide 'mes-dev-hs-new)
;;; mes-dev-hs-new.el ends here
