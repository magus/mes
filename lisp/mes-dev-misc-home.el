;;; mes-dev-misc-home.el --- Setup for development in various languages (home) -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(when (eq mes-location 'home)
  (use-package kotlin-mode
    :hook (kotlin-mode . lsp-deferred)
    :general
    (mes/despot-def kotlin-mode-map
      "'" '("interactive" . kotlin-repl)
      "b" `("backend" . ,(make-sparse-keymap))
      "br" '("restart" . lsp-workspace-restart)
      "bs" '("shutdown" . lsp-workspace-shutdown)
      "s" `("repl" . ,(make-sparse-keymap))
      "sb" '("send buf" . kotlin-send-buffer)
      "sf" '("send def" . kotlin-send-block) ;; I'm not sure what a block is
      "sl" '("send line" . kotlin-send-line)
      "sr" '("send region" . kotlin-send-region)
      "sB" '("send buf & switch" . kotlin-send-buffer-and-focus)
      "sF" '("send def & switch" . kotlin-send-block-and-focus) ;; I'm not sure what a block is
      "sR" '("send region & switch" . kotlin-send-region-and-focus)))

  (use-package geiser-guile
    :general
    (mes/despot-def geiser-mode-map
      "'" '("interactive" . geiser-mode-switch-to-repl-and-enter)
      "e" '("eval sexp" . geiser-eval-last-sexp)
      "s" `("repl" . ,(make-sparse-keymap))
      "sb" '("send buf" . geiser-eval-buffer)
      "sB" '("send buf" . geiser-eval-buffer-and-go)))

  (use-package tuareg
    :hook (tuareg-mode . lsp-deferred)
    :general
    (mes/despot-def tuareg-mode-map
      "=" `("format" . ,(make-sparse-keymap))
      "=b" '("buffer" . lsp-format-buffer)
      "=r" '("region" . lsp-format-region)
      "a" `("code actions" . ,(make-sparse-keymap))
      "aa" '("code action" . lsp-execute-code-action)
      "al" '("lens" . lsp-avy-lens)
      "b" `("backend" . ,(make-sparse-keymap))
      "br" '("restart" . lsp-workspace-restart)
      "bs" '("shutdown" . lsp-workspace-shutdown)
      "g" `("goto" . ,(make-sparse-keymap))
      "gb" '("pop" . pop-tag-mark)
      "gd" '("goto def" . evil-goto-definition)
      "gr" '("find refs" . xref-find-references)
      "gt" '("find type def" . lsp-find-type-definition)
      "gT" '("goto type def" . lsp-goto-type-definition))))

(provide 'mes-dev-misc-home)
;;; mes-dev-misc-home.el ends here
