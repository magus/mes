;;; mes-dev-py.el --- Setup for python development -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package python
  :hook (python-mode . lsp-deferred)
  :custom
  (lsp-pylsp-plugins-mypy-enabled t)
  (lsp-pylsp-plugins-ruff-enabled t)
  :config
  (defun mes/py-shell-send-buffer-switch ()
    "Send buffer content to repl and switch to it in insert mode."
    (interactive)
    (let ((python-mode-hook nil))
      (python-shell-send-buffer)
      (python-shell-switch-to-shell)
      (evil-insert-state)))
  (defun mes/py-shell-send-defun-switch ()
    "Send function definition to repl and switch to it in insert mode."
    (interactive)
    (let ((python-mode-hook nil))
      (python-shell-send-defun nil)
      (python-shell-switch-to-shell)
      (evil-insert-state)))
  (defun mes/py-shell-send-region-switch (start end)
    "Send region to shell and switch to it in insert mode."
    (interactive)
    (let ((python-mode-hook nil))
      (python-shell-send-region start end)
      (python-shell-switch-to-shell)
      (evil-insert-state)))
  :general
  (mes/despot-def python-mode-map
    "'" '("interactive" . run-python)
    "=" `("format" . ,(make-sparse-keymap))
    "=b" '("buffer" . lsp-format-buffer)
    "=r" '("region" . lsp-format-region)
    "=i" '("imports" . lsp-organize-imports)
    "a" `("code actions" . ,(make-sparse-keymap))
    "aa" '("code action" . lsp-execute-code-action)
    "b" `("backend" . ,(make-sparse-keymap))
    "br" '("restart" . lsp-workspace-restart)
    "bs" '("shutdown" . lsp-workspace-shutdown)
    ;; "c" `("execute" . ,(make-sparse-keymap))
    ;; "d" `("debug" . ,(make-sparse-keymap))
    "g" `("goto" . ,(make-sparse-keymap))
    "gb" '("pop" . pop-tag-mark)
    "gd" '("goto def" . evil-goto-definition)
    "gr" '("find refs" . xref-find-references)
    "h" `("help" . ,(make-sparse-keymap))
    "hd" '("describe" . lsp-describe-thing-at-point)
    "i" '("indent" . indent-tools-hydra/body)
    "r" `("refactor" . ,(make-sparse-keymap))
    "rr" '("rename" . lsp-rename)
    "s" `("repl" . ,(make-sparse-keymap))
    "sb" '("send buf" . python-shell-send-buffer)
    "se" '("send statement" . python-shell-send-statement)
    "sf" '("send def" . python-shell-send-defun)
    "sr" '("send region" . python-shell-send-region)
    ;; "ss" '("send w/ o" . python-shell-send-region-with-output)
    "sB" '("send buf & switch" . mes/py-shell-send-buffer-switch)
    "sF" '("send def & switch" . mes/py-shell-send-defun-switch)
    "sR" '("send region & switch" . mes/py-shell-send-region-switch)
    ;; "t" `("test" . ,(make-sparse-keymap))
    ;; "v" `("virtualenv" . ,(make-sparse-keymap))
    ;; "x" `("text/code" . ,(make-sparse-keymap))
    ;; "F" `("folder" . ,(make-sparse-keymap))
    ;; "G" `("peek" . ,(make-sparse-keymap))
    ;; "S" `("sphinx-doc" . ,(make-sparse-keymap))
    "T" `("toggle" . ,(make-sparse-keymap))
    "Tl" '("lsp" . lsp)))

(provide 'mes-dev-py)
;;; mes-dev-py.el ends here
