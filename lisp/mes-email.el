;;; mes-email.el --- Email (mu) setup -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package mu4e
  :straight (:type built-in)
  :commands mu4e
  :init
  (setq mu4e-completing-read-function #'completing-read
        mu4e-use-fancy-chars t
        mu4e-view-show-images t
        message-kill-buffer-on-exit t
        mu4e-org-support nil)
  (let ((dir "~/Downloads"))
    (when (file-directory-p dir)
      (setq mu4e-attachment-dir dir)))
  :config
  (setq
   mail-user-agent 'mu4e-user-agent
   mu4e-update-interval nil
   mu4e-headers-skip-duplicates t
   mu4e-headers-include-related t
   mu4e-sent-messages-behavior 'delete
   mu4e-html2text-command 'mu4e-shr2text
   mu4e-headers-sort-direction 'ascending
   mu4e-headers-sort-field :date
   mu4e-headers-date-format "%Y-%m-%d"
   mu4e-view-show-addresses t
   mu4e-headers-results-limit -1
   mu4e-compose-format-flowed t
   message-signature (lambda ()
                       (let ((cmd-string (concat "create-sig mail "
                                                 (mu4e-context-name (mu4e-context-current)))))
                         (setq message-signature (shell-command-to-string cmd-string))))

   mu4e-bookmarks `((:name "Inbox" :query "tag:\\\\Inbox" :key ?i)
                    (:name "Flagged messages" :query "flag:flagged" :key ?f)
                    (:name"Unread messages"
                     :query ,(string-join (append '("flag:unread"
                                                    "NOT flag:trashed"
                                                    "NOT maildir:/home/[Gmail].Spam"
                                                    "NOT maildir:/home/[Gmail].Bin")
                                                  (when (eq mes-location 'work)
                                                    '("NOT maildir:/carboncloud/[Gmail].Spam"
                                                      "NOT maildir:/carboncloud/[Gmail].Bin")))
                                          " AND ")
                     :key ?u))

   mu4e-maildir (expand-file-name "~/.maildir")
   mu4e-get-mail-command (string-join (append '("offlineimap -a Home")
                                              (when (eq mes-location 'work)
                                                '("CarbonCloud")))
                                      ",")
   mu4e-context-policy 'pick-first

   mu4e-contexts `(,(make-mu4e-context
                     :name "home"
                     :enter-func (lambda () (progn
                                              (mu4e-message "Entering home context")
                                              (add-hook 'mu4e-compose-mode-hook
                                                        'mml-secure-message-sign-pgpmime)))
                     :leave-func (lambda () (progn
                                              (mu4e-message "Leaving home context")
                                              (remove-hook 'mu4e-compose-mode-hook
                                                           'mml-secure-message-sign-pgpmime)))
                     :match-func (lambda (msg)
                                   (when msg
                                     (string-match-p "^/home" (mu4e-message-field msg :maildir))))
                     :vars '((user-mail-address . "magnus@therning.org")
                             (smtpmail-smtp-user . "magnus@therning.org")
                             (mu4e-sent-folder . "/home/[Gmail].All Mail")
                             (mu4e-drafts-folder . "/home/[Gmail].Drafts")
                             (mu4e-trash-folder . "/home/[Gmail].Bin")
                             (mu4e-refile-folder . "/home/[Gmail].All Mail")))))

  (when (eq mes-location 'work)
    (add-to-list 'mu4e-contexts
                 (make-mu4e-context
                  :name "carboncloud"
                  :enter-func (lambda () (mu4e-message "Entering carboncloud context"))
                  :leave-func (lambda () (mu4e-message "Leaving carboncloud context"))
                  :match-func (lambda (msg)
                                (when msg
                                  (string-match-p "^/carboncloud" (mu4e-message-field msg :maildir))))
                  :vars '((user-mail-address . "magnus.therning@carboncloud.com")
                          (smtpmail-smtp-user . "magnus.therning@carboncloud.com")
                          (mu4e-sent-folder . "/carboncloud/[Gmail].All Mail")
                          (mu4e-drafts-folder . "/carboncloud/[Gmail].Drafts")
                          (mu4e-trash-folder . "/carboncloud/[Gmail].Bin")
                          (mu4e-refile-folder . "/carboncloud/[Gmail].All Mail")))))

  (add-to-list 'mu4e-headers-actions '("retag" . mu4e-action-retag-message))
  (add-to-list 'mu4e-view-actions '("retag" . mu4e-action-retag-message))
  (add-to-list 'mu4e-marks
               '(tag-order
                 :char "$"
                 :prompt "order"
                 :ask-target nil
                 :action (lambda (docid msg target)
                           (mu4e-action-retag-message msg "-\\Inbox,+current-orders"))))

  (setq
   message-send-mail-function 'smtpmail-send-it
   starttls-use-gnutls t
   smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
   smtpmail-default-smtp-server "smtp.gmail.com"
   smtpmail-smtp-server "smtp.gmail.com"
   smtpmail-smtp-service 587
   smtpmail-debug-info t)

  (setq
   mml-secure-openpgp-sign-with-sender t
   mml-attach-file-at-the-end t)

  (add-hook 'mu4e-compose-mode-hook
            (lambda () (use-hard-newlines t 'guess)))
  (add-hook 'mu4e-mark-execute-pre-hook
            (lambda (mark msg)
              (cond ((equal mark 'refile) (mu4e-action-retag-message msg "-\\Inbox"))
                    ((equal mark 'trash) (mu4e-action-retag-message msg "-\\Inbox,-\\Starred"))
                    ((equal mark 'flag) (mu4e-action-retag-message msg "-\\Inbox,\\Starred"))
                    ((equal mark 'unflag) (mu4e-action-retag-message msg "-\\Starred")))))
  (defun mes/mu4e-view-mark (flag)
    (mu4e--view-in-headers-context
     (mu4e-headers-mark-and-next 'tag-order)))
  :general
  (mes/tyrant-def
    "am" '("email" . mu4e))
  (mes/despot-def mu4e-compose-mode-map
    "c" '("send & exit" . message-send-and-exit)
    "f" '("attach file" . mml-attach-file)
    "h" `("headers" . ,(make-sparse-keymap))
    "hb" '("bcc" . message-goto-bcc)
    "hc" '("cc" . message-goto-cc)
    "hf" '("from" . message-goto-from)
    "hF" '("followup-to" . message-goto-followup-to)
    "hk" '("keywords" . message-goto-keywords)
    "ht" '("to" . message-goto-to)
    "hs" '("subj" . message-goto-subject)
    "hr" '("reply" . message-goto-reply-to)
    "k" '("abort" . message-kill-buffer)
    "s" '("don't send" . message-dont-send))
  (mes/despot-def mu4e-headers-mode-map
    "m" `("mark" . ,(make-sparse-keymap))
    "mf" '("flag" . mu4e-headers-mark-for-flag)
    "mu" '("unmark" . mu4e-headers-mark-for-unmark)
    "mU" '("unmark all" . mu4e-mark-unmark-all)
    "ms" '("something" . mu4e-headers-mark-for-something)
    "mo" '("order" . (lambda () (interactive) (mu4e-headers-mark-and-next 'tag-order)))
    "u" '("update" . mu4e-update-mail-and-index))
  (mes/despot-def mu4e-view-mode-map
    "U" `("urls" . ,(make-sparse-keymap))
    "UX" '("fetch url" . mu4e-view-fetch-url)
    "Ux" '("open url" . mu4e-view-go-to-url)
    "c" `("compose" . ,(make-sparse-keymap))
    "cc" '("new" . mu4e-compose-new)
    "ce" '("edit" . mu4e-compose-edit)
    "cf" '("fwd" . mu4e-compose-forward)
    "cr" '("reply" . mu4e-compose-reply)
    "m" `("mark" . ,(make-sparse-keymap))
    "mU" '("unmark all" . mu4e-view-unmark-all)
    "mf" '("flag" . mu4e-view-mark-for-flag)
    "mo" '("order" . (lambda () (interactive) (mes/mu4e-view-mark 'tag-order)))
    "ms" '("something" . mu4e-view-mark-for-something)
    "mu" '("unmark" . mu4e-view-unmark)
    "s" '("save attachments" . mu4e-view-save-attachments)
    "u" '("update" . mu4e-update-mail-and-index)))

(provide 'mes-email)
;;; mes-email.el ends here
