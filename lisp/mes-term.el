;;; mes-term.el --- Shell and terminal setup -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package shell
  :hook
  (shell-mode . pcomplete-shell-setup)
  :config
  (add-to-list 'evil-normal-state-modes 'shell-mode))

(use-package pcmpl-args)

(use-package terminal-here
  :custom
  (terminal-here-linux-terminal-command 'foot)
  :general
  (mes/tyrant-def
    "$" '("terminal" . terminal-here)
    "p$" '("proj terminal" . terminal-here-project-launch)))

(provide 'mes-term)
;;; mes-term.el ends here
