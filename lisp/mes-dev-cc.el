;;; mes-dev-cc.el --- Setup for C++ development -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package cc-mode
  :hook
  ((c-mode c++-mode) . (lambda () (c-set-style "my-cc-style")))
  ((c-mode c++-mode) . lsp-deferred)
  :config
  (c-add-style "my-cc-style" '("cc-mode" (c-offsets-alist . ((innamespace . 0)))))
  :general
  (mes/despot-def c++-mode-map
    "=" `("format" . ,(make-sparse-keymap))
    "=b" '("buffer" . lsp-format-buffer)
    "=r" '("region" . lsp-format-region)
    ;; "=i" '("imports" . lsp-organize-imports)
    "a" `("code actions" . ,(make-sparse-keymap))
    "aa" '("code action" . lsp-execute-code-action)
    "b" `("backend" . ,(make-sparse-keymap))
    "br" '("restart" . lsp-workspace-restart)
    "bs" '("shutdown" . lsp-workspace-shutdown)
    ;; "c" `("execute" . ,(make-sparse-keymap))
    ;; "d" `("debug" . ,(make-sparse-keymap))
    "g" `("goto" . ,(make-sparse-keymap))
    "gb" '("pop" . pop-tag-mark)
    "gd" '("goto def" . evil-goto-definition)
    "gr" '("find refs" . xref-find-references)
    "h" `("help" . ,(make-sparse-keymap))
    "hd" '("describe" . lsp-describe-thing-at-point)
    "i" '("indent" . indent-tools-hydra/body)
    "r" `("refactor" . ,(make-sparse-keymap))
    "rr" '("rename" . lsp-rename)
    ;; "s" `("repl" . ,(make-sparse-keymap))
    ;; "sb" '("send buf" . python-shell-send-buffer)
    ;; "se" '("send statement" . python-shell-send-statement)
    ;; "sf" '("send def" . python-shell-send-defun)
    ;; "sl" '("send line" . python-shell-send-line)
    ;; "sr" '("send region" . python-shell-send-region)
    ;; "ss" '("send w/ o" . python-shell-send-region-with-output)
    ;; "sB" '("send buf & switch" . mes/py-shell-send-buffer-switch)
    ;; "sF" '("send def & switch" . mes/py-shell-send-defun-switch)
    ;; "sR" '("send region & switch" . mes/py-shell-send-region-switch)
    ;; "t" `("test" . ,(make-sparse-keymap))
    ;; "v" `("virtualenv" . ,(make-sparse-keymap))
    ;; "x" `("text/code" . ,(make-sparse-keymap))
    ;; "F" `("folder" . ,(make-sparse-keymap))
    ;; "G" `("peek" . ,(make-sparse-keymap))
    ;; "S" `("sphinx-doc" . ,(make-sparse-keymap))
    "T" `("toggle" . ,(make-sparse-keymap))
    "Tl" '("lsp" . lsp)))

;; TODO: I'd prefer to use the cmake-ts-mode that comes with Emacs at some point
(use-package cmake-mode
  :hook (cmake-mode . lsp-deferred))

(use-package cmake-font-lock
  :after cmake-mode
  :config (cmake-font-lock-activate))

(provide 'mes-dev-cc)
;;; mes-dev-cc.el ends here
