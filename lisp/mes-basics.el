;;; mes-basics.el --- The basic settings -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file :no-error-if-file-missing)

(setq-default fill-column 80)
(column-number-mode)
(setq display-line-numbers-type 'relative
      display-line-numbers-width 3)
(dolist (mode '(prog-mode-hook
                org-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))
(add-to-list 'warning-suppress-types '(comp))

;; Set fonts
(setq frame-inhibit-implied-resize t
      frame-resize-pixelwise t)
(set-face-attribute 'default nil :font "Iosevka" :height 110 :weight 'normal)
(set-face-attribute 'fixed-pitch nil :font "Iosevka" :height 110 :weight 'normal)
(set-face-attribute 'variable-pitch nil :font "Cantarell" :height 110 :weight 'regular)

(customize-set-variable 'global-auto-revert-non-file-buffers t)
(global-auto-revert-mode 1)
(delete-selection-mode)
(setopt indent-tabs-mode nil)
(setq electric-indent-inhibit t)
(setq use-short-answers t)
(setq sentence-end-double-space nil)

(add-hook 'after-init-hook #'recentf-mode)
(customize-set-variable 'recentf-save-file
                        (expand-file-name ".cache/recentf" user-emacs-directory))
(setq-default recentf-max-saved-items 100
              recentf-max-menu-items 20
              recentf-exclude '("/tmp" "/ssh:"))
(add-hook 'after-init-hook #'save-place-mode)

(customize-set-variable 'kill-do-not-save-duplicates t)
(setq-default tab-bar-show 1)

;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(customize-set-variable 'fast-but-imprecise-scrolling t)
(customize-set-variable 'scroll-conservatively 101)
(customize-set-variable 'scroll-margin 5)
(customize-set-variable 'scroll-preserve-screen-position t)
(pixel-scroll-precision-mode 1)
(setopt pixel-scroll-precision-large-scroll-height 40.0)

(setopt switch-to-buffer-obey-display-actions t
        switch-to-buffer-in-dedicated-window 'pop)

;; Better support for files with long lines
(setq-default bidi-paragraph-direction 'left-to-right)
(setq-default bidi-inhibit-bpa t)
(global-so-long-mode 1)

;; Enable savehist-mode for an command history
(savehist-mode 1)
(customize-set-variable 'savehist-file
                        (expand-file-name ".cache/history" user-emacs-directory))
(setq bookmark-save-flag 1)

(set-default-coding-systems 'utf-8)

;; turn on some handy stuff that's off by default
(put 'erase-buffer 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)

;; configure VC
(setopt vc-handled-backends nil)

(provide 'mes-basics)
;;; mes-basics.el ends here
