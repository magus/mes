;;; mes-dev-misc-work.el --- Setup for development in various languages (work) -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(when (eq mes-location 'work)
  (use-package elm-mode
    :hook (elm-mode . lsp-deferred)
    :general
    (mes/despot-def elm-mode-map
      "=" `("format" . ,(make-sparse-keymap))
      "=b" '("buffer" . elm-format-buffer)
      "=r" '("region" . elm-format-region)
      "a" `("code actions" . ,(make-sparse-keymap))
      "aa" '("code action" . lsp-execute-code-action)
      "al" '("lens" . lsp-avy-lens)
      "b" `("backend" . ,(make-sparse-keymap))
      "br" '("restart" . lsp-workspace-restart)
      "bs" '("shutdown" . lsp-workspace-shutdown)
      "g" `("goto" . ,(make-sparse-keymap))
      "gb" '("pop" . pop-tag-mark)
      "gd" '("goto def" . evil-goto-definition)
      "gr" '("find refs" . xref-find-references)
      "gt" '("find type def" . lsp-find-type-definition)
      "gT" '("goto type def" . lsp-goto-type-definition)))

  (use-package typescript-ts-mode
    :mode (("\\.ts\\'" . typescript-ts-mode)
           ("\\.tsx\\'" . tsx-ts-mode))
    :hook
    (typescript-ts-mode . lsp-deferred)
    (tsx-ts-mode . lsp-deferred)
    :init
    (add-to-list 'treesit-language-source-alist '(typescript "https://github.com/tree-sitter/tree-sitter-typescript" nil "typescript/src"))
    (add-to-list 'treesit-language-source-alist '(tsx "https://github.com/tree-sitter/tree-sitter-typescript" nil "tsx/src"))
    ;; (treesit-install-language-grammar 'typescript)
    ;; (treesit-install-language-grammar 'tsx)
    (add-to-list 'major-mode-remap-alist '(typescript-mode . typescript-ts-mode))
    (defalias 'typescript-mode 'typescript-ts-mode)
    :general
    (mes/despot-def
      :keymaps '(typescript-ts-mode-map tsx-ts-mode-map)
      "=" `("format" . ,(make-sparse-keymap))
      "=b" '("buffer" . lsp-format-buffer)
      "=r" '("region" . lsp-format-region)
      "a" `("code actions" . ,(make-sparse-keymap))
      "aa" '("code action" . lsp-execute-code-action)
      "b" `("backend" . ,(make-sparse-keymap))
      "br" '("restart" . lsp-workspace-restart)
      "bs" '("shutdown" . lsp-workspace-shutdown)
      "g" `("goto" . ,(make-sparse-keymap))
      "gb" '("pop" . pop-tag-mark)
      "gd" '("goto def" . evil-goto-definition)
      "gr" '("find refs" . xref-find-references)
      "h" `("help" . ,(make-sparse-keymap))
      "hd" '("describe" . lsp-describe-thing-at-point)
      "i" '("indent" . indent-tools-hydra/body)
      "r" `("refactor" . ,(make-sparse-keymap))
      "rr" '("rename" . lsp-rename)
      "T" `("toggle" . ,(make-sparse-keymap))
      "Tl" '("lsp" . lsp)))

  (use-package terraform-mode
    :custom
    (terraform-command "tofu")
    :general
    (mes/despot-def terraform-mode-map
      "h" '("open doc" . terraform-open-doc)
      "g" `("goto" . ,(make-sparse-keymap))
      "g[" '("start of fun" . hcl-beginning-of-defun)
      "g]" '("end of fun" . hcl-end-of-defun)
      "=" `("format" . ,(make-sparse-keymap))
      "=b" '("buffer" . terraform-format-buffer)
      "=r" '("region" . terraform-format-region)))

  (use-package go-ts-mode
    :hook
    (go-ts-mode . lsp-deferred)
    (go-ts-mode . go-format-on-save-mode)
    :init
    (add-to-list 'treesit-language-source-alist '(go "https://github.com/tree-sitter/tree-sitter-go"))
    (add-to-list 'treesit-language-source-alist '(gomod "https://github.com/camdencheek/tree-sitter-go-mod"))
    ;; (dolist (lang '(go gomod)) (treesit-install-language-grammar lang))
    ;; (add-to-list 'major-mode-remap-alist '(go-mode . go-ts-mode))
    ;; (add-to-list 'major-mode-remap-alist '(go-mode-mode . go-mod-ts-mode))
    :config
    (reformatter-define go-format
      :program "goimports"
      :args '("/dev/stdin"))
    :general
    (mes/despot-def go-ts-mode-map
      "=" `("format" . ,(make-sparse-keymap))
      "=b" '("buf" . lsp-format-buffer)
      "=i" '("imports" . lsp-organize-imports)
      "=r" '("region" . lsp-format-region)
      "a" `("code actions" . ,(make-sparse-keymap))
      "aa" '("code action" . lsp-execute-code-action)
      "al" '("lens" . lsp-avy-lens)
      "b" `("backend" . ,(make-sparse-keymap))
      "bd" '("describe" . lsp-describe-session)
      "br" '("restart" . lsp-workspace-restart)
      "bs" '("shutdown" . lsp-workspace-shutdown)
      "g" `("goto" . ,(make-sparse-keymap))
      "gb" '("pop" . pop-tag-mark)
      "gd" '("goto def" . lsp-find-definition)
      "gr" '("find refs" . lsp-find-references)
      "gt" '("find type def" . lsp-find-type-definition)
      "gi" '("find impls" . lsp-find-implementation)
      "h" `("help" . ,(make-sparse-keymap))
      "hd" '("describe" . lsp-describe-thing-at-point)
      "r" `("refactor" . ,(make-sparse-keymap))
      "rr" '("rename" . lsp-rename))))

(provide 'mes-dev-misc-work)
;;; mes-dev-misc-work.el ends here
