;;; mes-latex.el --- LaTeX setup -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package auctex
  :hook
  (LaTeX-mode . TeX-fold-mode)
  (LaTeX-mode . LaTeX-math-mode)
  (LaTeX-mode . TeX-source-correlate-mode)
  (LaTeX-mode . TeX-PDF-mode)
  (LaTeX-mode . (lambda () (flycheck-select-checker 'tex-lacheck)))
  :init
  (setq TeX-auto-save t
        TeX-parse-self t
        TeX-syntactic-comment t
        TeX-source-correlate-start-server nil
        LaTeX-fill-break-at-separators nil)
  (setq-default TeX-master nil)
  :custom
  (TeX-view-program-selection '((output-pdf "xdg-open") (output-html "xdg-open")))
  :config
  (defun mes/tex-force-latex (func)
    (let ((TeX-command-force "LaTeX"))
      (funcall func)))
  :general
  (mes/despot-def LaTeX-mode-map
    "C" '("clean" . TeX-clean)
    "c" `("cmds" . ,(make-sparse-keymap))
    "ca" '("all" . TeX-command-run-all)
    "cb" '("buffer" . (lambda () (interactive) (mes/tex-force-latex #'TeX-command-buffer)))
    "cm" '("master" . (lambda () (interactive) (mes/tex-force-latex #'TeX-command-master)))
    "v" '("view" . TeX-view)))

(use-package evil-tex
  :hook
  (LaTeX-mode . evil-tex-mode)
  :init
  (setq evil-tex-toggle-override-m nil
        evil-tex-toggle-override-t nil))

(provide 'mes-latex)
;;; mes-latex.el ends here
