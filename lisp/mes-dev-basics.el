;;; mes-dev-basics.el --- Setup for development -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package emacs
  :hook
  (prog-mode . electric-pair-local-mode)
  :custom
  (compilation-scroll-output 'first-error)
  :general
  (mes/despot-def prog-mode-map
    "m" `("move" . ,(make-sparse-keymap))
    "m^" '("beg fun" . beginning-of-defun)
    "m$" '("end fun" . end-of-defun)
    "mj" '("down list" . down-list)
    "ml" '("fwd list" . forward-list)
    "mh" '("back list" . backword-list)
    "mk" '("up list" . backward-up-list)
    "mh" '("back sexp" . backward-sexp)
    "ml" '("fwd sexp" . forward-sexp)
    ;; mark-defun
    ;; transpose-sexps
    ))

(use-package aggressive-indent)

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-traditional)
  (magit-define-global-key-bindings nil)
  :init
  (setq forge-add-default-bindings nil)
  :config
  (put 'magit-clean 'disabled nil)
  :general
  (mes/tyrant-def
    "gc" '("clone" . magit-clone)
    "gd" '("dispatch" . magit-dispatch)
    "gf" `("file" . ,(make-sparse-keymap))
    "gfd" '("file dispatch" . magit-file-dispatch)
    "gfl" '("file log" . magit-log-buffer-file)
    "gi" '("init" . magit-init)
    "gl" '("log" . magit-log)
    "gb" '("blame" . magit-blame)
    "gs" '("status" . magit-status))
  (mes/despot-def magit-mode-map
    "e" '("edit" . magit-edit-thing)
    "m" '("forge merge" . forge-merge)
    "o" '("open/browse" . magit-browse-thing))
  (mes/despot-def git-commit-mode-map
    "a" '("ack" . git-commit-ack)
    "c" '("finish" . with-editor-finish)
    "k" '("cancel" . with-editor-cancel)
    "s" '("signoff" . git-commit-signoff))
  (mes/despot-def git-rebase-mode-map
    "c" '("finish" . with-editor-finish)
    "k" '("cancel" . with-editor-cancel))
  (mes/despot-def magit-log-select-mode-map
    "c" '("pick" . magit-log-select-pick)
    "k" '("cancel" . magit-log-select-quit)))

(use-package forge
  :after magit
  :hook (magit-mode . forge-bug-reference-setup)
  :general
  (mes/despot-def forge-post-mode-map
    "c" '("submit" . forge-post-submit)
    "k" '("cancel" . forge-post-cancel))
  (mes/despot-def forge-post-section-map
    "k" '("delete" . forge-delete-comment))
  (mes/despot-def forge-topic-mode-map
    "n" '("create" . forge-create-post))
  (mes/despot-def magit-mode-map
    "c" '("copy url" . forge-copy-url-at-point-as-kill)))

(use-package pr-review
  :after magit
  :init
  (defun mes/pr-review-via-forge ()
    (interactive)
    (if-let* ((target (forge--browse-target))
              (url (if (stringp target) target (forge-get-url target)))
              (rev-url (pr-review-url-parse url)))
        (pr-review url)
      (user-error "No PR to review at point")))
  :general
  (general-def pr-review-mode-map
    :states 'normal
    "RET" 'pr-review-context-comment
    "a" 'pr-review-context-action
    "e" 'pr-review-context-edit
    "o" 'pr-review-open-in-default-browser
    "R" 'pr-review-refresh)
  (general-def pr-review-mode-map
    :states 'visual
    "RET" 'pr-review-context-comment)
  (mes/despot-def pr-review-input-mode-map
    "c" '("confirm" . pr-review-input-exit)
    "k" '("cancel" . pr-review-input-abort)
    "@"	'("mention user" . pr-review-input-mention-user))
  (mes/tyrant-def
    "gn" '("gh notifications" . pr-review-notification))
  (mes/despot-def magit-mode-map
    "R" '("pr review" . pr-review)
    "n" '("gh notifications" . pr-review-notification)
    "r" '("pr review" . mes/pr-review-via-forge)))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook
  (lsp-mode . lsp-enable-which-key-integration)
  (lsp-completion-mode . (lambda () (setq-local completion-category-defaults nil)))
  :custom
  (lsp-lens-place-position 'above-line)
  (lsp-auto-execute-action nil)
  (lsp-enable-suggest-server-download nil)
  :config
  (setq lsp-completion-provider :none)
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\.go-build\\'"))

(use-package lsp-ui
  :config
  (lsp-ui-sideline-mode 1)
  (lsp-ui-doc-mode 1)
  :custom
  (lsp-ui-sideline-show-code-actions nil)
  (lsp-ui-sideline-show-diagnostics t)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-delay 0.5)
  (lsp-ui-doc-position 'bottom)
  (lsp-ui-doc-show-with-cursor t)
  (lsp-ui-doc-show-with-mouse nil)
  (lsp-ui-doc-delay 0.5))

(use-package flycheck
  :commands (flycheck-list-errors flycheck-get-error-list-window)
  :init (global-flycheck-mode)
  :config
  (defun mes/toggle-flycheck-error-list ()
    (interactive)
    (if-let* ((window (flycheck-get-error-list-window)))
        (save-selected-window (quit-window nil window))
      (flycheck-list-errors)))
  :general
  (mes/tyrant-def
    "ej" '("next" . flycheck-next-error)
    "ek" '("previous" . flycheck-previous-error)
    "el" '("list" . mes/toggle-flycheck-error-list)))

(use-package flycheck-projectile
  :config
  (defun mes/toggle-flycheck-projectile-error-list ()
    (interactive)
    (if-let* ((window (get-buffer-window flycheck-projectile-error-list-buffer)))
        (save-selected-window (quit-window nil window))
      (flycheck-projectile-list-errors)))

  :general
  (mes/tyrant-def
    "ep" '("project" . mes/toggle-flycheck-projectile-error-list)))

(use-package ws-butler
  :hook ((org-mode prog-mode) . ws-butler-mode)
  :custom
  (ws-butler-keep-whitespace-before-point nil))

(use-package sh-script
  :hook
  (after-save . executable-make-buffer-file-executable-if-script-p))

(use-package smerge-mode
  :pretty-hydra
  (hydra-smerge (:title "Smerge" :quit-key "q" :post (smerge-auto-leave))
                ("Move" (("j" smerge-next "dn")
                         ("k" smerge-prev "up"))
                 "Keep" (("b" smerge-keep-base "base")
                         ("u" smerge-keep-upper "upper")
                         ("l" smerge-keep-lower "lower")
                         ("a" smerge-keep-all "all")
                         ("RET" smerge-keep-current "current"))
                 "Diff" (("<" smerge-diff-base-upper "base/upper")
                         ("=" smerge-diff-upper-lower "upper/lower")
                         (">" smerge-diff-base-lower "base/lower")
                         ("R" smerge-refine "refine")
                         ("E" smerge-ediff "ediff"))
                 "Other" (("C" smerge-combine-with-next "combine")
                          ("r" smerge-resolve "resolve")
                          ("K" smerge-kill-current "kill"))))
  :hook (magit-diff-visit-file . (lambda () (when smerge-mode (hydra-smerge/body)))))

(use-package ediff
  :hook (ediff-prepare-buffer . show-all)
  :custom
  (ediff-window-setup-function #'ediff-setup-windows-plain)
  (ediff-split-window-function #'split-window-horizontally)
  (ediff-merge-split-window-function #'split-window-horizontally)
  :general
  (mes/tyrant-def
    "D" `("diff/merge" . ,(make-sparse-keymap))
    "Db" `("buffers" . ,(make-sparse-keymap))
    "DbD" '("3-way diff" . ediff-buffers3)
    "DbM" '("3-way merge" . ediff-merge-buffers-with-ancestor)
    "Dbd" '("2-way diff" . ediff-buffers)
    "Dbm" '("2-way merge" . ediff-merge-buffers)
    "Dd" `("dirs" . ,(make-sparse-keymap))
    "DdD" '("3-way diff" . ediff-directories3)
    "DdM" '("3-way merge" . ediff-merge-directories-with-ancestor)
    "Ddd" '("2-way diff" . ediff-directories)
    "Ddm" '("2-way merge" . ediff-merge-directories)
    "Df" `("files" . ,(make-sparse-keymap))
    "DfD" '("3-way diff" . ediff-files3)
    "DfM" '("3-way merge" . ediff-merge-files-with-ancestor)
    "Dfd" '("2-way diff" . ediff-files)
    "Dfm" '("2-way merge" . ediff-merge-files)
    "Dh" '("doc" . ediff-documentation)))

(use-package indent-tools)

(use-package indent-bars
  :straight (:host github
             :repo "jdtsmith/indent-bars")
  :hook ((python-mode yaml-mode) . indent-bars-mode)
  :custom
  (indent-bars-display-on-blank-lines nil))

(use-package reformatter
  :straight (:host github
             :repo "purcell/emacs-reformatter"))

(use-package treesit-fold
  :init
  (global-treesit-fold-indicators-mode 1)
  (add-to-list 'treesit-fold-range-alist `(haskell-ng-mode . ,(treesit-fold-parsers-haskell))))

(use-package combobulate)

(use-package uuidgen
  :general
  (mes/tyrant-def
    "xu" '("uuid-4" . uuidgen)))

(use-package consult-lsp
  :general
  (mes/tyrant-def
    "pi" '("idx" . consult-lsp-symbols)))

(provide 'mes-dev-basics)
;;; mes-dev-basics.el ends here
