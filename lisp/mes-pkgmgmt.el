;;; mes-pkgmgmt.el --- Setup of package management, i.e. straight -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; get straight.el
(defvar bootstrap-version)
(setq straight-repository-branch "develop")
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; install use-package
(straight-use-package 'use-package)

(use-package straight
  :config
  (straight-use-package 'org)
  :custom
  (straight-use-package-by-default t))

(use-package use-package
  :custom
  (setq use-package-always-defer t))

(provide 'mes-pkgmgmt)
;;; mes-pkgmgmt.el ends here
