;;; mes-org.el --- Orgmode setup -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defun mes/org-mode-setup ()
  (org-indent-mode)
  ;; (variable-pitch-mode 1) ; proportional font
  (display-line-numbers-mode 0)
  (setq-local truncate-lines nil
              evil-shift-width 2
              imenu-auto-rescan t))

(use-package org
  :hook (org-mode . mes/org-mode-setup)
  :init
  (defmacro mes/org-emphasize (char)
    `(lambda () (interactive)
       (org-emphasize ,char)))
  :config
  (dolist (mod '(org-id org-habit ob-async ob-dot))
    (add-to-list 'org-modules mod))
  (dolist (pl '(dot python shell sql))
    (add-to-list 'org-babel-load-languages `(,pl . t)))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-link-frame-setup '(file . find-file))
  (add-to-list 'org-file-apps '("\\.pdf\\'" . "xdg-open %s"))
  (setq org-babel-min-lines-for-block-output 1) ; to force an example block on all verbatim results
  :custom
  (calendar-week-start-day 1)
  (org-directory (expand-file-name "~/org-notes"))
  (org-agenda-files '())
  (org-confirm-babel-evaluate '())
  (org-log-into-drawer t)
  (org-enforce-todo-checkbox-dependencies t)
  ;; (org-id-link-to-org-use-id t)
  (org-id-link-to-org-use-id 'use-existing)
  (org-startup-indented t)
  (org-edit-src-content-indentation 0)
  (org-footnote-auto-adjust t)
  (org-startup-with-inline-images t)
  (org-insert-heading-respect-content t)
  (org-goto-interface 'outline-path-completion)
  (org-outline-path-complete-in-steps nil)
  (org-startup-folded 'content)
  :general
  (mes/tyrant-def
    "ao" `("org" . ,(make-sparse-keymap))
    "aoa" '("org agenda" . org-agenda)
    "aoc" '("org capture" . org-capture)
    "aoi" '("org idx" . (lambda () (interactive) (find-file "~/org-notes/roam/index.org")))
    "aol" '("org store link" . org-store-link))
  (mes/despot-def org-mode-map
    "RET" '("C-c -ret" . org-ctrl-c-ret)
    "'" '("edit spec" . org-edit-special)
    "*" '("C-c *" . org-ctrl-c-star)
    "," '("C-c C-c" . org-ctrl-c-ctrl-c)
    "-" '("C-c -" . org-ctrl-c-minus)
    "#" '("update stats" . org-update-statistics-cookies)
    "g" '("goto" . org-goto)
    "p" '("prio" . org-priority)
    "A" '("attach" . org-attach)
    "H" '("shift left" . org-shiftleft)
    "J" '("shift down" . org-shiftdown)
    "K" '("shift up" . org-shiftup)
    "L" '("shift right" . org-shiftright)
    ;; babel
    "b" `("babel" . ,(make-sparse-keymap))
    "bb" '("exe buf" . org-babel-execute-buffer)
    "bc" '("check block" . org-babel-check-src-block)
    "bd" '("rm res" . org-babel-remove-result)
    "bD" '("rm all res" . org-babel-remove-result-one-or-many)
    "be" '("exe" . org-babel-execute-maybe)
    "bg" '("goto named block" . org-babel-goto-named-src-block)
    "bj" '("goto next block" . org-babel-next-src-block)
    "bk" '("goto prev block" . org-babel-previous-src-block)
    "br" '("goto named res" . org-babel-goto-named-result)
    "bs" '("exe sub" . org-babel-execute-subtree)
    "bv" '("exp block" . org-babel-expand-src-block)
    ;; dates
    "d" `("dates" . ,(make-sparse-keymap))
    "dt" '("time stamp" . org-time-stamp)
    "dT" '("inactive time stamp" . org-time-stamp-inactive)
    ;; export
    "e" '("export" . org-export-dispatch)
    ;; insert
    "i" `("insert" . ,(make-sparse-keymap))
    "ib" '("block" . org-insert-structure-template)
    "if" '("footnote" . org-footnote-new)
    "ih" '("heading" . org-insert-heading)
    "iH" '("heading after" . org-insert-heading-after-current)
    "il" '("link" . org-insert-link)
    "in" '("note" . org-add-note)
    "ip" '("prop" . org-set-property)
    "is" '("subheading" . org-insert-subheading)
    "l" `("list" . ,(make-sparse-keymap))
    "lr" '("repair" . org-list-repair)
    ;; trees/subtrees
    "s" `("trees/subtrees" . ,(make-sparse-keymap))
    "sh" '("promote sub" . org-promote-subtree)
    "sj" '("mv sub down" . org-move-subtree-down)
    "sk" '("mv sub up" . org-move-subtree-up)
    "sl" '("demote sub" . org-demote-subtree)
    "sn" '("narrow sub" . org-narrow-to-subtree)
    "sw" 'widen
    "sy" '("cp sub" . org-copy-subtree)
    "ss" '("sort" . org-sort)
    ;; tables
    "t" `("tables" . ,(make-sparse-keymap))
    "tH" '("mv col ←" . org-table-move-column-left)
    "tJ" '("mv row ↓" . org-table-move-row-down)
    "tK" '("m row ↑" . org-table-move-row-up)
    "tL" '("mv col →" . org-table-move-column-right)
    "ta" '("align" . org-table-align)
    "th" '("←" . org-table-previous-field)
    "tj" '("↓" . org-table-next-row)
    ;; "tk" '("↑" . org-table-previous-row)
    "tl" '("→" . org-table-next-field)
    "tn" '("create" . org-table-create)
    "ts" '("sort lines" . org-table-sort-lines)
    "tt" '("transpose" . org-table-transpose-table-at-point)
    "td" `("delete" . ,(make-sparse-keymap))
    "tdc" '("del col" . org-table-delete-column)
    "tdr" '("del row" . org-table-kill-row)
    "ti" `("insert" . ,(make-sparse-keymap))
    "tih" '("ins col ←" . org-table-insert-column)
    "tij" '("ins row ↑" . (lambda () (interactive) (org-table-insert-row 0)))
    "tik" '("ins row ↑" . org-table-insert-row)
    "til" '("ins col →" . (lambda () (interactive) (org-table-insert-column) (org-table-move-column-right)))
    ;; toggles
    "T" `("toggles" . ,(make-sparse-keymap))
    "Tc" '("checkbox" . org-toggle-checkbox)
    "Ti" '("image" . org-toggle-inline-images)
    "Tl" '("link" . org-toggle-link-display)
    "Tt" '("todo" . org-todo)
    ;; text
    "x" `("text" . ,(make-sparse-keymap))
    "xb" `("italic". ,(mes/org-emphasize ?*))
    "xc" `("code". ,(mes/org-emphasize ?~))
    "xi" `("italic". ,(mes/org-emphasize ?/))
    "xr" `("clear". ,(mes/org-emphasize ? ))
    "xs" `("strike". ,(mes/org-emphasize ?+))
    "xu" `("underline". ,(mes/org-emphasize ?_))
    "xv" `("verbatim". ,(mes/org-emphasize ?=)))
  (general-def org-mode-map
    :states 'normal
    "RET" 'org-open-at-point)
  (mes/despot-def org-src-mode-map
    "c" 'org-edit-src-exit
    "k" 'org-edit-src-abort)
  (mes/despot-def org-capture-mode-map
    "c" 'org-capture-finalize
    "k" 'org-capture-kill))

(use-package org-modern
  :hook
  (org-mode . org-modern-mode)
  (org-agenda-finalize . org-modern-agenda))

(use-package org-contrib)

(use-package org-re-reveal)

(use-package org-roam
  :custom
  (org-roam-directory (expand-file-name "~/org-notes/roam"))
  (org-roam-dailies-directory (expand-file-name "~/org-notes/roam/dailies"))
  (org-roam-dailies-capture-templates `(("d" "daily" plain "%?"
                                         :target (file+head
                                                  "%<%Y-%m-%d>.org"
                                                  ,(string-join '("#+title: %<%Y-%m-%d>"
                                                                  "#+filetags: :journal:"
                                                                  "#+todo: TODO(t/!) | DONE(d@) CANCELLED(c@)"
                                                                  "\n") "\n"))
                                         :immediate-finish t)))
  (org-roam-capture-templates `(("d" "default" plain "%?"
                                 :target (file+head
                                          "%<%Y%m%d%H%M%S>-${slug}.org"
                                          ,(string-join '("#+title: ${title}"
                                                          "#+filetags: %^{Tags}\n\n") "\n"))
                                 :unnarrowed t)
                                ("r" "reading" plain "%?"
                                 :target (file+head
                                          "%<%Y%m%d%H%M%S>-${slug}.org"
                                          ,(string-join '(":PROPERTIES:"
                                                          ":ROAM_REFS: %^{Key}"
                                                          ":END:"
                                                          "#+title: ${title}"
                                                          "#+filetags: %^{Tags}\n\n") "\n"))
                                 :unnarrowed t)))
  (org-roam-node-display-template "${title:70} ${tags:30}")
  (org-roam-db-node-include-function (lambda () (not (member "ATTACH" (org-get-tags)))))
  :config
  (org-roam-db-autosync-mode 1)
  :general
  (mes/despot-def org-mode-map
    ;; roam
    "r" `("roam" . ,(make-sparse-keymap))
    "rf" 'org-roam-node-find
    "ri" 'org-roam-node-insert
    ;; roam - dailies
    "rd" `("dailies" . ,(make-sparse-keymap))
    "rdd" '("date" . org-roam-dailies-goto-date)
    "rdt" '("today" . org-roam-dailies-goto-today)
    "rdT" '("tomorrow" . org-roam-dailies-goto-tomorrow)
    "rdy" '("yesterday" . org-roam-dailies-goto-yesterday)
    ;; roam -> tags
    "rt" `("tags" . ,(make-sparse-keymap))
    "rta" '("add" . org-roam-tag-add)
    "rtd" '("rm" . org-roam-tag-remove)))

(use-package org-static-blog)

(use-package ob-async)

(use-package ob-http
  :straight (:host github :repo "ag91/ob-http")
  :config
  (add-to-list 'org-babel-load-languages '(http . t)))

(use-package ob-mermaid)

(use-package mep-roam-extras
  :straight (:type git
             :repo "git@gitlab.com:magus/mep-roam-extras.git"
             :branch "main")
  :hook
  (before-save . mep-roam-extras-update-todo-tag)
  (org-roam-find-file . mep-roam-extras-update-todo-tag)
  :init (advice-add 'org-agenda :before #'mep-roam-extras-update-todo-files))

(provide 'mes-org)
;;; mes-org.el ends here
