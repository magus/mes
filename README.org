* My (very own) Emacs Setup

I was watching some of the videos aobut [[https://www.youtube.com/watch?v=74zOY-vgkyw&list=PLEoMzSkcN8oPH1au7H6B7bBJ4ZO7BXjSZ][Emacs From Scratch]] and thought that it
didn't look as hard as I'd thought. It looked fun, even.

It turned out to not be that difficult. Also, it was a bit of fun. At first it
felt like it'd be a lot of work to get a configuration that could rival my
[[https://github.com/syl20bnr/spacemacs][Spacemacs]] setup so I stopped and walked away. After a few months I came back to
it with the intention of trying out [[https://github.com/joaotavora/eglot][eglot]] for a language or two (I ended up
dropping it after a while though). At that point I couldn't stop myself and
ended up making this configuration good enough that I've now dropped Spacemacs
completely.

* High-level description

The basic building blocks are

- [[https://github.com/radian-software/straight.el][straight.el]]
- [[https://github.com/noctuid/general.el][general.el]]
- [[https://github.com/emacs-evil/evil][evil]]
- [[https://github.com/minad/vertico][vertico]]/[[https://github.com/minad/consult][consult]]/...

On top of that I've then added the stuff I use on a daily basis

- [[https://www.djcbsoftware.nl/code/mu/mu4e.html][mu4e]]
- [[https://orgmode.org/][org mode]]
- [[https://emacs-lsp.github.io/lsp-mode/][lsp-mode]] and some languages ([[https://github.com/haskell/haskell-mode][haskell]], [[https://github.com/dominikh/go-mode.el][golang]], ...)

* What's left?

See the [[file:TODO.org][TODO]] for a list of things I've left to add or explore.
